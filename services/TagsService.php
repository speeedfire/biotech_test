<?php

namespace app\services;

use app\controllers\ProductTagsController;
use app\domains\ar\ProductTags;

class TagsService
{
    public $productId;
    public $tags;

    public function saveTags()
    {
        $this->deleteOldTags();

        foreach ($this->tags as $tag)
        {
            $model = new ProductTags();
            $model->idtags = $tag;
            $model->idproduct = $this->productId;
            $model->save(false);
        }
    }

    private function deleteOldTags()
    {
        $productTagsService = new ProductTagsService();
        $productTagsService->deleteOldTags($this->productId);
    }
}