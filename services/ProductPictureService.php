<?php


namespace app\services;


use app\domains\ar\ProductPicture;

class ProductPictureService
{
    public $primaryKey;
    public $pictureAlt;
    public $pictureName;
    public $scenario = 'create';

    public function save()
    {
        if($this->scenario == 'update')
        {
            $pictureModel = ProductPicture::findOne($this->primaryKey);
        }
        else
        {
            $pictureModel = new ProductPicture();
        }

        if($this->pictureName)
        {
            $pictureModel->picture_name = 'assets/products/' . $this->pictureName;
        }

        $pictureModel->picture_alt = $this->pictureAlt;
        $pictureModel->save(false);

        $this->primaryKey = $pictureModel->primaryKey;
    }

    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    public static function delete($id)
    {
        ProductPicture::findOne($id)->delete();
    }
}