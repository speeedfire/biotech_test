<?php

namespace app\services;

use yii\web\UploadedFile;

class PictureUploadService
{
    /**
     * @var UploadedFile
     */
    public $picture;

    /**
     * upload pic
     */
    public function upload()
    {
        $this->picture->saveAs('..' . DIRECTORY_SEPARATOR . 'web'  . DIRECTORY_SEPARATOR .  'assets' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR . $this->picture->baseName . '.' . $this->picture->extension);
    }
}