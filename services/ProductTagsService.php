<?php


namespace app\services;

use app\domains\ar\ProductTags;

class ProductTagsService
{
    public $productId;

    public function deleteOldTags($productId)
    {
        self::delete($productId);
    }

    public static function delete($productId)
    {
        ProductTags::deleteAll('idproduct = :idproduct', [
            ':idproduct' => $productId
        ]);
    }
}