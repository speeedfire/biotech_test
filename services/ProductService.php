<?php
namespace app\services;


use app\domains\ar\Languages;
use app\domains\ar\ProductAttributes;
use app\domains\ar\ProductPicture;
use app\domains\ar\Products;
use app\domains\ar\ProductTags;
use app\domains\ProductFormModel;
use yii\web\UploadedFile;

class ProductService
{
    /**
     * @var $model \app\domains\ProductFormModel
     */
    public $model;
    public $primaryKey;
    public $isNew = true;

    /**
     * ProductService constructor.
     * @param $model
     */
    public function __construct($model)
    {
        $this->model = $model;
        $this->setIsNew();
    }

    /*
     * set the model isNew prop
     */
    private function setIsNew()
    {
        if($this->model->scenario == 'update') {
            $this->isNew = false;
        }
    }

    /**
     * get primary key
     * @return integer
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * delete model
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete()
    {
        ProductTagsService::delete($this->model->primaryKey);
        ProductPictureService::delete(ProductAttributes::findOne(['product_id' => $this->model->primaryKey])->product_picture_id);
        ProductAttributesService::delete(ProductAttributes::findOne(['product_id' => $this->model->primaryKey, 'product_language_id' =>$this->model->languageId])->primaryKey);
        if (!ProductAttributes::findOne(['product_id' => $this->model->primaryKey]))
        {
            Products::findOne($this->model->primaryKey)->delete();
        }
    }

    /**
     * save the model
     * @return bool
     */
    public function save()
    {
        /**
         * save the product
         */
        if($this->isNew) {
            $productModel = new Products();
        }
        else
        {
            $productModel = Products::findOne($this->model->primaryKey);
        }

        $productModel->valid_from = $this->model->validFrom;
        $productModel->valid_to = $this->model->validTo;
        $productModel->created_by = 1; //used the admin
        $productModel->product_name = $this->model->productName;
        $productModel->create_date = date('Y-m-d H:i:s');
        $productModel->save(false);

        /**
         * save the product pic
         */
        $picUploadService = new PictureUploadService();
        $picUploadService->picture = $this->model->picture;
        if($picUploadService->picture instanceof UploadedFile) {
            $picUploadService->upload();
        }

        $productPicture = new ProductPictureService();
        if(!$this->isNew) {
            $productPicture->scenario = 'update';
            $productPicture->primaryKey = ProductAttributes::findOne(['product_id' => $this->model->primaryKey])->primaryKey;
        }
        $productPicture->pictureAlt = $this->model->pictureAlt;
        if($picUploadService->picture instanceof UploadedFile) {
            $productPicture->pictureName = $this->model->picture;
        }
        $productPicture->save();

        /**
         * save tags
         */
        $productTagsService = new TagsService();
        $productTagsService->productId = $productModel->primaryKey;
        $productTagsService->tags = $this->model->tags;
        $productTagsService->saveTags();


        /**
         * save product attributes
         */
        $productAttr = new ProductAttributesService();

        if(!$this->isNew) {
            $productAttr->scenario = 'update';
        }
        $productAttr->productId = $productModel->primaryKey;
        $productAttr->productPrice = $this->model->productPrice;
        $productAttr->productPicId = $productPicture->getPrimaryKey();
        $productAttr->productLangId = $this->model->languageId;
        $productAttr->productName = $this->model->productLocalName;
        $productAttr->productDesc = $this->model->productDesc;
        $productAttr->save(false);

        $this->primaryKey = $productModel->primaryKey;

        return true;

    }

    /**
     * load the model
     * @param integer $id
     * @param string $lang
     * @return ProductFormModel
     */
    public static function load($id, $lang = 'en')
    {
        $model = Products::findOne($id);
        $modelAttr = ProductAttributes::findOne(['product_id' => $id, 'product_language_id' => Languages::findOne(['short_name' => $lang])->primaryKey]);
        $modelPic = ProductPicture::findOne(['idproduct_picture' => $modelAttr->product_picture_id]);
        $modelLang = Languages::findOne(['idlanguages' => $modelAttr->product_language_id]);
        $modelTags = ProductTags::findAll(['idproduct' => $id]);

        $tags = [];
        foreach ($modelTags as $tag)
        {
            $tags[] = $tag->idtags;
        }

        $formModel = new ProductFormModel();
        $formModel->scenario = 'update';
        $formModel->picture = $modelPic->picture_name;
        $formModel->productDesc = $modelAttr->product_desc;
        $formModel->productLocalName = $modelAttr->product_name;
        $formModel->productName = $model->product_name;
        $formModel->languageId = $modelAttr->product_language_id;
        $formModel->productPrice = $modelAttr->product_price;
        $formModel->pictureAlt = $modelPic->picture_alt;
        $formModel->tags = $tags;
        $formModel->validFrom = $model->valid_from;
        $formModel->validTo = $model->valid_to;

        $formModel->primaryKey = $id;

        return $formModel;
    }
}