<?php


namespace app\services;


use app\domains\ar\ProductAttributes;

class ProductAttributesService
{
    public $productId;
    public $productDesc;
    public $productPrice;
    public $productName;
    public $productLangId;
    public $productPicId;
    public $scenario = 'create';

    public function save()
    {
        if($this->scenario == 'update')
        {
            $model = ProductAttributes::findOne(['product_id' => $this->productId, 'product_language_id' => $this->productLangId]);
        }
        else
        {
            $model = new ProductAttributes();
        }

        $model->product_name = $this->productName;
        $model->product_desc = $this->productDesc;
        $model->product_language_id = $this->productLangId;
        $model->product_picture_id = $this->productPicId;
        $model->product_id = $this->productId;
        $model->product_price = $this->productPrice;

        $model->save(false);
    }

    public static function delete($id)
    {
        ProductAttributes::findOne($id)->delete();
    }
}