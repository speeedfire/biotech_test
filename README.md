Az alkalmazás docker alól fut, így ez szükséges hozzá.

~~~~
Le kell húzni a repót, minden változás a master-be benne van.
~~~~

`git checkout master`

Majd futtatni kell a docker container-t.

`docker-compose up -d`

Ezután be kell lépni a container-be, futtatni a composer update-et, és a migrációkat.

Linux alatt:

`docker exec -it biotech-test_php_1 bash`

Windows alatt:

`winpty docker exec -it biotech-test_php_1 bash`

A container alatt pedig ezeket:

`composer update`

`yii migrate up`

Ezek után böngészőből el lehet érni az alkalmazást már.

http://localhost:8000/index.php?r=products

Alapaból felvettem 2 terméket, az egyikhez van 2 féle lokalizáció is.
Nincs minden teljesen befejezve, illetve nagyon sokat lehetne még rajta módosítani, illetve teszteket írni hozzá. De ez elég sok idő még.

Bármi kérdés van: info@szabolcs-toth.com