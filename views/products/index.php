<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Products', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'idproducts',
                'value'=>'products.idproducts',
            ],
            [
                'attribute' => 'valid_from',
                'value'=>'products.valid_from',
            ],
            [
                'attribute' => 'valid_to',
                'value'=>'products.valid_to',
            ],
            [
                'attribute' => 'create_date',
                'value'=>'products.create_date',
            ],
            [
                'attribute' => 'product_name',
                'value'=>'products.product_name',
            ],
            'product_price',
            'product_name',
            'product_language',

            //'created_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'urlCreator' => function( $action, $model, $key, $index ){
                    if ($action == "view") {
                        return Url::to(['view', 'id' => $model->product_id, 'lang' => $model->product_short_language]);
                    }
                    if ($action == "update") {
                        return Url::to(['update', 'id' => $model->product_id, 'lang' => $model->product_short_language]);
                    }
                    if ($action == "delete") {
                        return Url::to(['delete', 'id' => $model->product_id, 'lang' => $model->product_short_language]);
                    }
                }
            ],
        ],
    ]); ?>


</div>
