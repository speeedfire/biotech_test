<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use kartik\select2\Select2;
use app\domains\ar\Languages;
use app\domains\ar\Tags;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'validFrom')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'validTo')->widget(\yii\jui\DatePicker::classname(), [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'languageId')->dropDownList(ArrayHelper::map(Languages::find()->all(),'idlanguages','language_name')) ?>

    <?= $form->field($model, 'productName')->textInput() ?>

    <?= $form->field($model, 'productLocalName')->textInput() ?>

    <?= $form->field($model, 'productPrice')->textInput() ?>

    <?= $form->field($model, 'picture')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
    ]); ?>

    <?php
        if(isset($model->primaryKey)) {
            echo '<img src="/' . $model->picture . '">';
        }
    ?>

    <?= $form->field($model, 'pictureAlt')->textInput() ?>

    <?= $form->field($model, 'tags')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Tags::find()->all(),'idtags','tag_name'),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a state ...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'productDesc')->widget(CKEditor::className(),[
        'editorOptions' => [
        'preset' => 'full',
        'inline' => false,
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>