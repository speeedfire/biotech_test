<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = $model->idproducts;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idproducts, 'lang' => Yii::$app->request->get('lang')], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idproducts, 'lang' => Yii::$app->request->get('lang')], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idproducts',
            'create_date',
            'valid_from',
            'valid_to',
            'product_name:ntext',
        ],
    ]) ?>

    <?= DetailView::widget([
        'model' => $modelAttr,
        'attributes' => [
            'product_desc:ntext',
            'product_price',
            'product_name:ntext',
        ],
    ]) ?>

    <?= DetailView::widget([
        'model' => $modelLang,
        'attributes' => [
            'language_name:ntext',
            'short_name:ntext',
        ],
    ]) ?>

    <?php
            foreach ($modelTags as $tag)
            {
               echo DetailView::widget([
                    'model' => $tag,
                    'attributes' => [
                        'tagName:ntext',
                    ],
                ]) ;
            }
    ?>

    <img src="/<?= $modelPic->picture_name ?>">

</div>
