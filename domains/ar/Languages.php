<?php

namespace app\domains\ar;

use Yii;

/**
 * This is the model class for table "languages".
 *
 * @property int $idlanguages
 * @property string|null $language_name
 * @property string|null $short_name
 */
class Languages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['language_name', 'short_name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idlanguages' => 'Idlanguages',
            'language_name' => 'Language Name',
            'short_name' => 'Short Name',
        ];
    }
}
