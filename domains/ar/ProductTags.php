<?php

namespace app\domains\ar;

use Yii;

/**
 * This is the model class for table "product_tags".
 *
 * @property int $idtags
 * @property int $idproduct
 */
class ProductTags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idtags', 'idproduct'], 'required'],
            [['idtags', 'idproduct'], 'integer'],
            [['idtags', 'idproduct'], 'unique', 'targetAttribute' => ['idtags', 'idproduct']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtags' => 'Idtags',
            'idproduct' => 'Idproduct',
        ];
    }

    public function getTagName()
    {
        return  Tags::findOne(['idtags' => $this->idtags])->tag_name;
    }
}
