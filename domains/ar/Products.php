<?php

namespace app\domains\ar;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $idproducts
 * @property string|null $create_date
 * @property string|null $valid_from
 * @property string|null $valid_to
 * @property string|null $product_name
 * @property int|null $created_by
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_date', 'valid_from', 'valid_to'], 'safe'],
            [['product_name'], 'string'],
            [['created_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idproducts' => 'Idproducts',
            'create_date' => 'Create Date',
            'valid_from' => 'Valid From',
            'valid_to' => 'Valid To',
            'product_name' => 'Product Name',
            'created_by' => 'Created By',
        ];
    }

    public function getProductattributes()
    {
        return $this->hasMany(ProductAttributes::className(), ['product_id' => 'idproducts']);
    }


}
