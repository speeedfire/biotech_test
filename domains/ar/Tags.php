<?php

namespace app\domains\ar;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property int $idtags
 * @property string|null $tag_name
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tag_name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtags' => 'Idtags',
            'tag_name' => 'Tag Name',
        ];
    }
}
