<?php

namespace app\domains\ar;

use Yii;

/**
 * This is the model class for table "product_picture".
 *
 * @property int $idproduct_picture
 * @property string|null $picture_alt
 * @property string|null $picture_name
 */
class ProductPicture extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_picture';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['picture_alt', 'picture_name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idproduct_picture' => 'Idproduct Picture',
            'picture_alt' => 'Picture Alt',
            'picture_name' => 'Picture Name',
        ];
    }
}
