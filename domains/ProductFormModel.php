<?php
namespace app\domains;

use Yii;
use yii\base\Model;

class ProductFormModel extends Model
{
    public $primaryKey;
    public $productName;
    public $validFrom;
    public $validTo;
    public $productDesc;
    public $productPrice;
    public $productLocalName;
    public $picture;
    public $pictureAlt;
    public $languageId;
    public $tags = [];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['productName', 'validFrom', 'validTo', 'productDesc', 'productPrice', 'productLocalName', 'pictureAlt', 'languageId', 'tags'], 'required', 'on' => 'create, update'],
            [['picture'], 'required', 'on' => 'create'],
            [['productName', 'productLocalName', 'pictureAlt'], 'string', 'length' => [1, 45]],
            [['productDesc'], 'string'],
            [['validFrom', 'validTo'], 'date', 'format' => 'yyyy-MM-dd'],
            [['picture'], 'image', 'skipOnEmpty' => true],
            [['productPrice'], 'double'],
            [['languageId'], 'integer'],
            [['tags'], 'each', 'rule' => ['integer']]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['productName', 'validFrom', 'validTo', 'productDesc', 'productPrice', 'productLocalName', 'pictureAlt', 'languageId', 'tags', 'picture'];
        $scenarios['update'] = ['productName', 'validFrom', 'validTo', 'productDesc', 'productPrice', 'productLocalName', 'pictureAlt', 'languageId', 'tags'];

        return $scenarios;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [

        ];
    }
}