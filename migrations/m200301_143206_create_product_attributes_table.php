<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_attributes}}`.
 */
class m200301_143206_create_product_attributes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_attributes}}', [
            'idproduct_attributes' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'product_desc' => $this->text(),
            'product_price' => $this->float(),
            'product_name' => $this->text(),
            'product_language_id' => $this->integer(),
            'product_picture_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_attributes}}');
    }
}
