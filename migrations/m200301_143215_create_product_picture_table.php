<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_picture}}`.
 */
class m200301_143215_create_product_picture_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_picture}}', [
            'idproduct_picture' => $this->primaryKey(),
            'picture_alt' => $this->text(),
            'picture_name' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_picture}}');
    }
}
