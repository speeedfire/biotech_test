<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_tags}}`.
 */
class m200301_143237_create_product_tags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_tags}}', [
            'idtags' => $this->integer()->notNull(),
            'idproduct' => $this->integer()->notNull()
        ]);

        $this->addPrimaryKey('product_tags_pk', 'product_tags', ['idtags', 'idproduct']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_tags}}');
    }
}
