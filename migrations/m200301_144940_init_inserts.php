<?php

use yii\db\Migration;

/**
 * Class m200301_144940_init_inserts
 */
class m200301_144940_init_inserts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $hash = Yii::$app->getSecurity()->generatePasswordHash('Abc123');

        /**
         * users
         */
        $this->insert('users', [
            'username' => 'admin',
            'password' => $hash,
        ]);

        $userId = Yii::$app->db->getLastInsertID();

        /**
         * tags
         */
        $this->insert('tags', [
           'tag_name' => 'feherje',
        ]);

        $tagFeherjeId = Yii::$app->db->getLastInsertID();

        $this->insert('tags', [
            'tag_name' => 'bcaa',
        ]);

        $tagBcaaId = Yii::$app->db->getLastInsertID();

        $this->insert('tags', [
            'tag_name' => 'marha',
        ]);

        $tagMarhaId = Yii::$app->db->getLastInsertID();

        /**
         * languages
         */
        $this->insert('languages', [
            'language_name' => 'english',
            'short_name' => 'en',
        ]);

        $langEnId = Yii::$app->db->getLastInsertID();

        $this->insert('languages', [
            'language_name' => 'hungarian',
            'short_name' => 'hu',
        ]);

        $langHuId = Yii::$app->db->getLastInsertID();

        /**
         * products
         */
        $this->insert('products', [
            'create_date' => date('Y-m-d H:i:s'),
            'valid_from' => date('Y-m-d H:i:s'),
            'valid_to' => date('Y-m-d H:i:s'),
            'product_name' => 'beef protein',
            'created_by' => $userId
        ]);

        $beefProductId = Yii::$app->db->getLastInsertID();

        $this->insert('products', [
            'create_date' => date('Y-m-d H:i:s'),
            'valid_from' => date('Y-m-d H:i:s'),
            'valid_to' => date('Y-m-d H:i:s'),
            'product_name' => 'bcaa',
            'created_by' => $userId
        ]);

        $bcaaProductId = Yii::$app->db->getLastInsertID();

        /**
         * product images
         */

        $this->insert('product_picture', [
            'picture_alt' => 'beef',
            'picture_name' => 'assets/products/beef.jpg'
        ]);

        $beefProductPciId = Yii::$app->db->getLastInsertID();

        $this->insert('product_picture', [
            'picture_alt' => 'beef2',
            'picture_name' => 'assets/products/beef2.jpg'
        ]);

        $beef2ProductPciId = Yii::$app->db->getLastInsertID();

        $this->insert('product_picture', [
            'picture_alt' => 'bcaa',
            'picture_name' => 'assets/products/bcaa.png'
        ]);

        $bcaaProductPciId = Yii::$app->db->getLastInsertID();


        /**
         * product_attributes
         */

        $this->insert('product_attributes', [
            'product_id' => $beefProductId,
            'product_desc' => 'Marha fehérhe stb',
            'product_price' => 3111,
            'product_name' => 'Marha fehérje',
            'product_language_id' => $langHuId,
            'product_picture_id' => $beefProductPciId
        ]);

        $this->insert('product_attributes', [
            'product_id' => $beefProductId,
            'product_desc' => 'Beef protein etc',
            'product_price' => 111,
            'product_name' => 'Beef protein',
            'product_language_id' => $langEnId,
            'product_picture_id' => $beef2ProductPciId
        ]);

        $this->insert('product_attributes', [
            'product_id' => $bcaaProductId,
            'product_desc' => 'Bcaa zero stb',
            'product_price' => 2221,
            'product_name' => 'Bcaa zero',
            'product_language_id' => $langHuId,
            'product_picture_id' => $bcaaProductPciId
        ]);

        $this->insert('product_tags', [
            'idtags' => $tagBcaaId,
            'idproduct' => $bcaaProductId
        ]);

        $this->insert('product_tags', [
            'idtags' => $tagFeherjeId,
            'idproduct' => $beefProductId
        ]);

        $this->insert('product_tags', [
            'idtags' => $tagMarhaId,
            'idproduct' => $beefProductId
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200301_144940_init_inserts cannot be reverted.\n";

        return false;
    }

}

