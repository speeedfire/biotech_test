<?php

namespace app\controllers;

use app\domains\ar\Languages;
use app\domains\ar\ProductAttributes;
use app\domains\ar\ProductPicture;
use app\domains\ar\ProductTags;
use app\services\ProductService;
use Yii;
use app\domains\ar\Products;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\domains\ProductFormModel;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ProductAttributes::find()->with('products'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @param string $lang
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $lang = 'en')
    {
        $model = $this->findModel($id);
        $modelAttr = ProductAttributes::findOne(['product_id' => $id, 'product_language_id' => Languages::findOne(['short_name' => $lang])->primaryKey]);
        $modelPic = ProductPicture::findOne(['idproduct_picture' => $modelAttr->product_picture_id]);
        $modelLang = Languages::findOne(['idlanguages' => $modelAttr->product_language_id]);
        $modelTags = ProductTags::findAll(['idproduct' => $id]);

        return $this->render('view', [
            'model' => $model,
            'modelAttr' => $modelAttr,
            'modelPic' => $modelPic,
            'modelLang' => $modelLang,
            'modelTags' => $modelTags,
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductFormModel();

        if ($model->load(Yii::$app->request->post())) {
            $model->picture =  UploadedFile::getInstance($model, 'picture');

            $productService = new ProductService($model);

            if ($model->validate() && $productService->save()) {
                $language = ProductAttributes::findOne(['product_id' => $productService->getPrimaryKey()]);

                return $this->redirect(['view', 'id' => $productService->getPrimaryKey(), 'lang' => $language->product_short_language]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $lang
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $lang = 'en')
    {
        $model = ProductService::load($id, $lang);

        if ($model->load(Yii::$app->request->post())) {
            $model->picture =  UploadedFile::getInstance($model, 'picture');
            $productService = new ProductService($model);

            if ($model->validate() && $productService->save()) {
                $language = ProductAttributes::findOne(['product_id' => $productService->getPrimaryKey()]);

                return $this->redirect(['view', 'id' => $productService->getPrimaryKey(), 'lang' => $language->product_short_language]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $lang
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $lang = 'en')
    {
        $model = ProductService::load($id, $lang);

        $service = new ProductService($model);
        $service->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
